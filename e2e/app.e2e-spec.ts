import { EliteFrontendPage } from './app.po';

describe('elite-frontend App', () => {
  let page: EliteFrontendPage;

  beforeEach(() => {
    page = new EliteFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
