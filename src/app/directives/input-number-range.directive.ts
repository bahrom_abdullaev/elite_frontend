import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidatorFn} from "@angular/forms";


@Directive({
  selector: '[appInputNumberRange]',
  providers: [{provide: NG_VALIDATORS, useExisting: InputNumberRangeDirective, multi: true}]
})
export class InputNumberRangeDirective {
  @Input() rangeNumbers: number[] = [0, 10];

  validate(control: AbstractControl): { [key: string]: any } {
    return this.rangeNumbers ? numberRangeValidator(this.rangeNumbers)(control)
      : null;
  }

}


export function numberRangeValidator(rangeNumbers: number[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const forbidden = rangeNumbers[0] > control.value || rangeNumbers[1] < control.value;
    return forbidden ? {'appInputNumberRange': {value: control.value}} : null;
  };
}
