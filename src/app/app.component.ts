import {Component, OnInit} from '@angular/core';
import {ExchangeService} from './services/exchange.service';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private exchangeService: ExchangeService) {

  }

  ngOnInit(): void {
    this.exchangeService.getPrimaryExchange();
  }

}
