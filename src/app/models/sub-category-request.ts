import {LISTING_PAGE_SIZE} from "../constants/app.constants";

export class SubCategoryRequest {
  constructor(public page = 1,
              public page_size = LISTING_PAGE_SIZE,
              public category: string = '',
              public search = '') {
  }

  public clear() {
    this.page = 1;
    this.page_size = LISTING_PAGE_SIZE;
    this.search = '';
    this.category = '';
  }

  getJSON() {
    return {
      page: this.page,
      page_size: this.page_size,
      search: this.search,
      category: this.category
    }
  }
}

