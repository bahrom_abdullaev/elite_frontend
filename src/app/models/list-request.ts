import {LISTING_PAGE_SIZE} from "../constants/app.constants";

export class ListRequest {
  constructor(public page = 1,
              public page_size = LISTING_PAGE_SIZE,
              public search = '') {
  }

  public clear() {
    this.page = 1;
    this.page_size = LISTING_PAGE_SIZE;
    this.search = '';
  }

  getJSON() {

    return {
      page: this.page,
      page_size: this.page_size,
      search: this.search
    }
  }
}

