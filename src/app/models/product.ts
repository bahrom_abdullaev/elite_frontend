export class Product {
  constructor(public id = '',
              public name = '',
              public category = '',
              public subcategory = '',
              public barcode = '',
              public amount = 0,
              public price_model = '') {
  }
}
