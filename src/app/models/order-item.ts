export class OrderItem {
  constructor(public id = '',
              public product = '',
              public store = '',
              public employee = '',
              public discount = 0,
              public quantity = 1,
              public status = '',
              public client = '',
              public sub_total = 0,
              public total = 0,
              public created_date = new Date(),
              public viewMode=false
  ) {
  }
}
