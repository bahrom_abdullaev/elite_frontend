export class Order {
  constructor(public id = '',
              public code = '',
              public store = '',
              public employee = '',
              public discount = 0,
              public status = '',
              public client = '',
              public sub_total = 0,
              public total = 0,
              public total_payment = 0,
              public total_discount = 0,
              public created_date = new Date()) {
  }
}
