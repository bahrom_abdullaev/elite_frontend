import {LISTING_PAGE_SIZE} from "../constants/app.constants";

export class InventoriesRequest {
  constructor(public page = 1,
              public page_size = LISTING_PAGE_SIZE,
              public search = '',
              public category: string = '',
              public subcategory: string = '',
              public warehouse: string = '',) {
  }

  public clear() {
    this.page = 1;
    this.page_size = LISTING_PAGE_SIZE;
    this.search = '';
    this.category = '';
    this.subcategory = '';
    this.warehouse = '';
  }

  getJSON() {
    return {
      page: this.page,
      page_size: this.page_size,
      search: this.search,
      category: this.category,
      warehouse: this.warehouse,
      subcategory: this.subcategory
    }
  }
}

