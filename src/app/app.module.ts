import {Router} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {HttpModule} from '@angular/http';
import {AuthModule} from './components/auth/auth.module';
import {DashboardModule} from './components/dashboard/dashboard.module';

import {AppComponent} from './app.component';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {LoaderComponent} from './components/popups/loader/loader.component';
import {PopupAlertComponent} from './components/popups/error-msg/alert.component';
import {InfiniteScrollerDirective} from './directives/infinite-scroller.directive';

import {CommonService} from './services/common.service';
import {BaseService} from './services/base.service';
import {LoaderService} from './services/loader.service';
import {PopupService} from './services/popup.service';
import {ReminderService} from './services/reminder.service';
import {EmployeeService} from './services/employee.service';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './services/auth-guard.service';
import {StoreService} from './services/store.service';
import {UserService} from './services/user.service';
import {ClientService} from './services/client.service';
import {InventoryService} from './services/inventory.service';
import {CategoryService} from './services/category.service';
import {ProductService} from './services/product.service';
import {ExchangeService} from './services/exchange.service';
import {OrderService} from './services/order.service';
import {MoneyPipe} from './pipes/money.pipe';
import { InputNumberRangeDirective } from './directives/input-number-range.directive';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NotFoundComponent,
    LoaderComponent,
    PopupAlertComponent,
    InputNumberRangeDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AuthModule,
    DashboardModule,
    AppRoutingModule,
    HttpModule,
  ],
  providers: [
    AuthGuard,
    CommonService,
    BaseService,
    LoaderService,
    PopupService,
    ReminderService,
    EmployeeService,
    AuthService,
    StoreService,
    UserService,
    ClientService,
    InventoryService,
    CategoryService,
    ProductService,
    ExchangeService,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
