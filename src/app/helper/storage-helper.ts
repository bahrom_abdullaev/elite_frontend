export function save(key, value) {
  const data = value ? JSON.stringify(value) : value;
  localStorage.setItem(key, data);
}
export function get(key) {
  const res = localStorage.getItem(key);
  return res ? JSON.parse(res) : res;
}
export function remove(key) {
  localStorage.removeItem(key);
}
