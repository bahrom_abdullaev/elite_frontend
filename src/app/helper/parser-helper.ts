import * as _ from "lodash";
export function formatMoney(value: any, n = 0, x?, s = ' '): string {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
  return parseFloat(value).toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&' + s);
}
export function parseMoney(value: any): number {
  return parseFloat(String(value).replace(/\s/g, ''));
}
export function currencyMoney(value: any, prefix = '', suffix = 'сўм'): string {
  return `${prefix} ${formatMoney(value)} ${suffix}`;
}
