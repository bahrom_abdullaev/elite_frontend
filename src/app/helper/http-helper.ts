import * as commonHelper from './common-helper';
export function handleError(error: any): Promise<any> {
  commonHelper.toggleLoader(false);

  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}
