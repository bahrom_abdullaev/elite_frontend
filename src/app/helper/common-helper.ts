import fp from 'lodash/fp';

export function toggleLoader(visible) {
  let searchStr = location.search || '';
  if (visible) {
    searchStr += (searchStr ? '&' : '?')+
    'loader=true';
  } else {
    searchStr.replace(/loader=true/gi, '')
  }
  // location.search = searchStr;
}
