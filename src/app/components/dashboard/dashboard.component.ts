import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ExchangeService} from '../../services/exchange.service';
import * as Constants from "../../constants/app.constants";
import {AuthService} from "../../services/auth.service";
import {OrderService} from "../../services/order.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {InventoryService} from "../../services/inventory.service";
import {ReminderService} from "../../services/reminder.service";
import {StoreService} from "../../services/store.service";
import {EmployeeService} from "../../services/employee.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public appName = Constants.APP_NAME;
  form: FormGroup;
  barcodeField = new FormControl('');

  constructor(public exchangeService: ExchangeService,
              public authService: AuthService,
              public location: Location,
              public router: Router,
              public employeeService: EmployeeService,
              public storeService: StoreService,
              public reminderService: ReminderService,
              public inventoryService: InventoryService,
              public orderService: OrderService,
              public formBuilder: FormBuilder,) {
    this.form = formBuilder.group({
      "barcodeField": this.barcodeField
    });
  }

  ngOnInit() {
  }

  getFormattedExchange() {
    return this.exchangeService.primaryExchange ? this.exchangeService.primaryExchange.summ : 0
  }

  openOrderList(path) {
    // this.orderService.list = [];
    // this.orderService.listCount = 0;
    this.router.navigate([path]);
  }

  isActive(path) {
    return this.location.path() == path;
  }

  barcodeSearch(search) {
    this.inventoryService.getInventoryList({search})
      .subscribe(res => {
        if (res) {
          if (res.results && res.results.length) {
            const item = res.results[0];
            item.viewMode = true;
            this.orderService.openOrderItemsPopup(item);
          }
        }
      });
  }

}
