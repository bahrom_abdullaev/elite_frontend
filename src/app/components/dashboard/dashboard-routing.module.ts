import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from '../../services/auth-guard.service'

import {DashboardComponent} from './dashboard.component';
import {OrderListComponent} from './order/order-list.component';
import {ClientsComponent} from './client/clients.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: OrderListComponent
      },
      {
        path: 'clients',
        component: ClientsComponent
      }
    ]
  }

];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
