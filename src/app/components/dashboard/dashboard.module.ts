import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DatePickerModule } from 'ng2-datepicker';
import { TextMaskModule } from 'angular2-text-mask';


import {SearchFormComponent} from './search/search-form.component';
import {BarcodeSearchFormComponent} from './search/barcode-search-form.component';
import {DashboardComponent} from './dashboard.component';
import {OrderListComponent} from './order/order-list.component';
import {ClientsComponent} from './client/clients.component';
import {ClientCreateComponent} from './popups/client-create.component';
import {ClientListComponent} from './client/client-list.component';
import {ClientOrderComponent} from './client/client-order.component';
import {OrderItemsComponent} from './order/order-items.component';
import {CategoryListComponent} from './client/category-list.component';
import {SubCategoryListComponent} from './client/sub-category-list.component';
import {ProductListComponent} from './client/product-list.component';
import {CategoryProductListComponent } from './client/category-product-list.component';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {InfiniteScrollerDirective} from '../../directives/infinite-scroller.directive';

import * as Constants from '../../constants/app.constants'
import {PopupClientListComponent} from "./popups/client-list.component";
import {PopupAddProductComponent} from "./popups/add-product.component";
import {PopupOrderPaymentComponent} from "./popups/order-payment.component";
import {PopupReminderListComponent} from "./popups/reminder-list.component";
import {MoneyPipe} from '../../pipes/money.pipe';
import {EliteMoneyPipe} from '../../pipes/elite-money.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    DatePickerModule,
    TextMaskModule
  ],
  declarations: [
    DashboardComponent,
    OrderListComponent,
    ClientsComponent,
    ClientListComponent,
    ClientOrderComponent,
    PopupClientListComponent,
    PopupOrderPaymentComponent,
    PopupReminderListComponent,
    SearchFormComponent,
    BarcodeSearchFormComponent,
    ClientCreateComponent,
    SubCategoryListComponent,
    ProductListComponent,
    CategoryProductListComponent,
    PopupAddProductComponent,
    CategoryListComponent,
    OrderItemsComponent,
    InfiniteScrollerDirective,
    MoneyPipe,
    EliteMoneyPipe
  ]
})
export class DashboardModule {
}
