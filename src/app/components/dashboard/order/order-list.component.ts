import {Component, OnDestroy, OnInit} from '@angular/core';
import {ListRequest} from "../../../models/list-request";
import {ClientService} from "../../../services/client.service";
import {OrderService} from "../../../services/order.service";
import {OrderStatus, TranslatedOrderStatus} from "../../../constants/app.constants";
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from "@angular/router";
import {OrderListRequest} from "../../../models/order-list-request";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ReminderService} from "../../../services/reminder.service";
import {flow, startCase, capitalize} from "lodash/fp"

@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit, OnDestroy {

  orderSearchForm: FormGroup;
  searchField = new FormControl('');


  list = [];
  categoryList = [];
  createPopup = false;
  locationPath: string;

  scrollCallback;

  listRequest = new OrderListRequest();

  constructor(public clientService: ClientService,
              public reminderService: ReminderService,
              public orderService: OrderService,
              private route: ActivatedRoute,
              private router: Router,
              formBuilder: FormBuilder,
              public location: Location) {
    // this.locationPath = location.path(true);

    this.scrollCallback = this.getListing.bind(this);
    this.orderSearchForm = formBuilder.group({
      "searchField": this.searchField
    });
    // this.searchField.valueChanges
    //   .subscribe(val => console.log(val));
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.orderService.list = [];
  }

  getStatusClass(item) {
    let className = 'label order-status-label ';
    switch (item.status) {
      case OrderStatus.DRAFT:
        className += 'label-primary';
        break;
      case OrderStatus.PARTIAL_PAID:
        className += 'label-warning';
        break;
      case OrderStatus.CANCELLED:
        className += 'label-danger';
        break;
      case OrderStatus.COMPLETED:
        className += 'label-success';
        break;
      default:
        className += 'label-default';
        break
    }
    return className;
  }

  getListing() {
    const orderId = this.route.snapshot.queryParams["orderId"];
    const list = this.orderService.list;
    if (!list.length || this.orderService.listCount - 1 >= list.length) {
      return this.orderService.getList(this.listRequest.getJSON())
        .do(this.processData)
    }

    return null;
  }

  public reloadList(isReset?) {
    this.orderService.item = null;
    if (isReset) {
      this.listRequest.clear();
      this.listRequest.status = [OrderStatus.DRAFT, OrderStatus.PARTIAL_PAID];
    }
    this.listRequest.page = 1;
    this.orderService.list = [];
    this.getListing().subscribe();
  }

  public searchFromListing(search) {
    this.orderService.item = null;

    this.listRequest.clear();
    this.listRequest.search = search;
    this.orderService.list = [];
    this.getListing().subscribe();
  }

  private processData = (list) => {
    this.listRequest.page++;
  };

  getStatusLabel(key) {
    return TranslatedOrderStatus[key];
  }

  public selectClient = (item) => {
    // this.orderService.selectOrderItem(null);
    setTimeout(() =>
        this.orderService.getOrder(item.id)
      , 10);
  };

  public openOrder = () => {
    const selectedId = this.orderService.item && this.orderService.item.id;
    this.router.navigate(['dashboard/clients'], {queryParams: {'left-view': 'client', 'orderId': selectedId}});
  };

  onSubmit() {
    this.searchFromListing(this.searchField.value);
  }
}
