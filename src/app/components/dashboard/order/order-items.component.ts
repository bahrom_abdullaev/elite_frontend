import {Component, OnInit, OnDestroy} from '@angular/core';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';

import {ClientService} from '../../../services/client.service'
import {ListRequest} from "../../../models/list-request";
import {OrderService} from "../../../services/order.service";


@Component({
  selector: 'order-items',
  templateUrl: './order-items.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderItemsComponent implements OnInit, OnDestroy {
  list = [];

  scrollCallback;

  listRequest = new ListRequest();

  constructor(public clientService: ClientService,
              public orderService: OrderService,
              public location: Location) {

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.orderService.setOrderItems();
  }

  getListing() {
    return null;
  }

  public searchFromListing(search) {
    this.listRequest.clear();
    this.listRequest.search = search;
    this.getListing();
  }

  public selectClient = (item) => {
    const selectedId = item && item.id;

    this.orderService.getOrderItemsItem(this.orderService.item.id, selectedId)
      .subscribe(res => {
        this.orderService.openOrderItemsPopup(res);
      });
  };
}
