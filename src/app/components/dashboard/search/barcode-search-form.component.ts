import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Location} from '@angular/common';
import {LoaderService} from "../../../services/loader.service";
import * as commonHelper from "../../../helper/common-helper";
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms'
import 'rxjs/Rx';

@Component({
  selector: 'barcode-search-form',
  templateUrl: './barcode-search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class BarcodeSearchFormComponent implements OnInit {
  @Output() onSubmitHandle = new EventEmitter();
  @Input() idName = 'search';
  form: FormGroup;
  barcodeField = new FormControl('');
  response = {};

  constructor(private location: Location,
              private loaderService: LoaderService,
              formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      "barcodeField": this.barcodeField
    });
    // this.form.valueChanges
    //   .filter(data => this.form.valid)
    //   .map(data => {
    //     return data;
    //   })
    //   .subscribe(data => console.log(JSON.stringify(data)));

  }

  ngOnInit() {
  }

  onSubmit() {
    this.onSubmitHandle.emit(this.barcodeField.value);
  }
}
