import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Location} from '@angular/common';
import {LoaderService} from "../../../services/loader.service";
import * as commonHelper from "../../../helper/common-helper";
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms'
import 'rxjs/Rx';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  @Output() onSubmitHandle = new EventEmitter();
  @Input() idName = 'search';
  @Input() placeholder = 'Поиск...';
  form: FormGroup;
  query = new FormControl('', Validators.required);
  response = {};

  constructor(private location: Location,
              private loaderService: LoaderService,
              formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      "query": this.query
    });
    // this.form.valueChanges
    //   .filter(data => this.form.valid)
    //   .map(data => {
    //     // data.phone = data.phone.replace(/<(?:.|\n)*?>/gm, '');
    //     return data;
    //   })
    //   .subscribe(data => console.log(JSON.stringify(data)));

  }

  ngOnInit() {
  }

  onSubmit() {
    // this.loaderService.toggleLoader(true);

    this.onSubmitHandle.emit(this.query.value);
  }
}
