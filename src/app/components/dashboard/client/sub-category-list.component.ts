import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {CategoryService} from '../../../services/category.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {SubCategoryRequest} from "../../../models/sub-category-request";
import {InventoriesRequest} from "../../../models/inventories-request";


@Component({
  selector: 'sub-category-list',
  templateUrl: './sub-category-list.component.html',
  styleUrls: ['./category.component.css']
})
export class SubCategoryListComponent implements OnInit {
  list = [];
  categoryList = [];
  createPopup = false;
  locationPath: string;

  scrollCallback;

  productsModel = new InventoriesRequest();
  listRequest = new SubCategoryRequest(1, 1000);

  constructor(public clientService: ClientService,
              public inventoryService: InventoryService,
              private categoryService: CategoryService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {
    this.locationPath = location.path(true);

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
    const params = this.router.parseUrl(this.location.path());
    const categoryId = params.queryParams.categoryId;
    if (!this.inventoryService.selectedCategory || this.inventoryService.selectedCategory != categoryId) {
      this.categoryService.getItem(categoryId)
        .subscribe(res => {
          this.inventoryService.selectedCategory = res;
        });
    }
    this.listRequest = new SubCategoryRequest();
    this.listRequest.category = params.queryParams.categoryId;

  }

  getListing() {
    return this.inventoryService.getSubCategoryList(this.listRequest.getJSON())
      .subscribe(this.processData);
  }

  public searchFromListing(search) {
    this.inventoryService.productList = [];
    const params = this.router.parseUrl(this.location.path());
    this.productsModel.search = search;
    this.productsModel.page = 1;
    this.inventoryService.getInventoryList({search, ...params.queryParams})
      .subscribe();
  }

  private processData = (list) => {
    this.listRequest.page++;
    this.list = this.list.concat(list);
  };

  public selectClient = (item,) => {
    const selectedId = item && item.id;
    this.inventoryService.selectCategory(item, true);

    const params = this.router.parseUrl(this.location.path());
    this.router.navigate([this.router.url.split('?')[0]], {
      queryParams: {
        ...params.queryParams,
        'right-view': 'products', 'subCategoryId': selectedId
      }
    })
  };

  goToMenu() {
    const params = this.router.parseUrl(this.location.path());
    delete params.queryParams['right-view'];
    delete params.queryParams['categoryId'];
    delete params.queryParams['subCategoryId'];
    this.router.navigate([this.router.url.split('?')[0]], {
      queryParams: {
        ...params.queryParams
      }
    })
  }
}
