import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {SubCategoryRequest} from "../../../models/sub-category-request";
import {InventoriesRequest} from "../../../models/inventories-request";
import {OrderService} from "../../../services/order.service";


@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./category.component.css']
})
export class ProductListComponent implements OnInit {
  list = [];
  createPopup = false;
  locationPath: string;

  scrollCallback;
  listRequest = new InventoriesRequest();

  constructor(public clientService: ClientService,
              public inventoryService: InventoryService,
              public orderService: OrderService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
    const params = this.router.parseUrl(this.location.path());

    // this.categoryService.getItem(categoryId)
    //   .subscribe(res => {
    //     this.inventoryService.selectedCategory = res;
    //   });
    this.listRequest = new InventoriesRequest();
    this.listRequest.category = params.queryParams.categoryId;
    this.listRequest.subcategory = params.queryParams.subCategoryId;

  }

  getListing() {
    return this.inventoryService.getInventoryList(this.listRequest.getJSON())
      .do(this.processData);
  }

  public searchFromListing(search) {
    this.inventoryService.productList = [];
    const params = this.router.parseUrl(this.location.path());
    this.listRequest.search = search;
    this.listRequest.page = 1;
    this.inventoryService.getInventoryList(this.listRequest.getJSON())
      .subscribe();
  }

  private processData = (res) => {
    this.listRequest.page++;
  };

  public selectItem = (item,) => {
    const selectedId = item && item.id;
    this.orderService.toggleOrderItemsPopup(true);

    // const params = this.router.parseUrl(this.location.path());
    // this.router.navigate([this.router.url.split('?')[0]], {
    //   queryParams: {
    //     ...params.queryParams,
    //     'right-view': 'products', 'subCategoryId': selectedId
    //   }
    // })
  };

  goToMenu() {
    const params = this.router.parseUrl(this.location.path());
    delete params.queryParams['right-view'];
    delete params.queryParams['categoryId'];
    delete params.queryParams['subCategoryId'];
    this.router.navigate([this.router.url.split('?')[0]], {
      queryParams: {
        ...params.queryParams
      }
    })
  }

  goToParent() {
    const cat = this.inventoryService.selectedCategory;
    if (cat) {
      const params = this.router.parseUrl(this.location.path());
      delete params.queryParams['subCategoryId'];

      this.router.navigate([this.router.url.split('?')[0]], {
        queryParams: {
          ...params.queryParams,
          'right-view': 'subCategory', 'categoryId': cat.id
        }
      })
    }
  }
}
