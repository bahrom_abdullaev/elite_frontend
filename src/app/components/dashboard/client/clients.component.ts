import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
declare var jquery: any;
declare var $: any;
import {OrderService} from "../../../services/order.service";


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  constructor(public orderService: OrderService,
              public location: Location) {
  }

  ngOnInit() {
    setTimeout(() => {
      const gH = $(document).outerHeight();
      const minHeight = gH - 60;
      if (minHeight > 480) {
        $('.side-pos').css({minHeight: minHeight});
      }
    }, 10);
  }
}
