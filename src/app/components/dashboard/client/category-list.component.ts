import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {ListRequest} from "../../../models/list-request";
import {InventoriesRequest} from "../../../models/inventories-request";


@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryListComponent implements OnInit {
  list = [];
  categoryList = [];
  createPopup = false;
  locationPath: string;

  scrollCallback;
  productsModel = new InventoriesRequest();
  listRequest = new ListRequest(1, 1000);

  constructor(public clientService: ClientService,
              private inventoryService: InventoryService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {
    this.locationPath = location.path(true);

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  getListing() {
    return this.inventoryService.getCategoryList(this.listRequest.getJSON())
      .subscribe(this.processData)
  }

  public searchFromListing(search) {
    this.inventoryService.productList = [];
    const params = this.router.parseUrl(this.location.path());
    this.productsModel.search = search;
    this.productsModel.page = 1;
    this.productsModel.category = params.queryParams.categoryId;
    this.inventoryService.getInventoryList(this.productsModel.getJSON())
      .subscribe();
  }

  private processData = (list) => {
    this.listRequest.page++;
    this.list = this.list.concat(list);
  };

  public selectClient = (item,) => {
    const selectedId = item && item.id;
    this.inventoryService.selectCategory(item);
    const params = this.router.parseUrl(this.location.path());
    this.router.navigate([this.router.url.split('?')[0]], {
      queryParams: {
        ...params.queryParams,
        'right-view': 'subCategory', 'categoryId': selectedId
      }
    })
  };
}
