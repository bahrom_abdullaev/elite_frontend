import {Component, OnInit, OnDestroy} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {ListRequest} from "../../../models/list-request";
import {CommonService} from "../../../services/common.service";
import {OrderService} from "../../../services/order.service";
import {ExchangeService} from "../../../services/exchange.service";

import * as _ from "lodash";
import {ReminderService} from "../../../services/reminder.service";
@Component({
  selector: 'client-order',
  templateUrl: './client-order.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientOrderComponent implements OnInit, OnDestroy {

  list = [];
  discountPrice = 0;
  createPopup = false;
  locationPath: string;

  scrollCallback;

  listRequest = new ListRequest();

  constructor(public clientService: ClientService,
              public reminderService: ReminderService,
              public orderService: OrderService,
              public exchangeService: ExchangeService,
              private inventoryService: InventoryService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {
  }

  ngOnInit() {
    const orderId = this.route.snapshot.queryParams["orderId"] || this.orderService.item.id;
    // if (!this.orderService.item) {
    this.orderService.getOrder(orderId, res => {
      this.discountPrice = _.round(this.exchangeService.getSOM(res.discount));
    });
    // } else {
    //   this.discountPrice = _.round(this.exchangeService.getSOM(this.orderService.item.discount));
    // }
  }

  ngOnDestroy(): void {
    this.orderService.selectOrderItem(null);
  }

  onSave() {
    if (this.orderService.item.discount != this.discountPrice) {
      const payload = {discount: this.exchangeService.getUSD(this.discountPrice)};
      this.orderService.changeOrder(this.orderService.item.id, payload);
    }
  }

  public selectClient = (item) => {
    const selectedId = item && item.id;
    this.clientService.selectItem(item);
    this.router.navigate(['dashboard/clients'], {queryParams: {'left-view': 'client', 'selected-id': selectedId}})
  };

  onKeyPressDiscount($event) {
    // const diff = positive ? 1000 : -1000;
    // this.discountPrice += diff;
    this.calcTotal();
  }

  toggleDiscount(positive: boolean) {
    const diff = positive ? 1000 : -1000;
    this.discountPrice += diff;
    this.calcTotal();
    this.onSave();
  }

  calcTotal() {
    const oldDiscount = this.orderService.item.discount;
    this.orderService.item.discount = this.exchangeService.getUSD(this.discountPrice);

    this.orderService.item.total_discount += this.orderService.item.discount - oldDiscount;
    this.orderService.item.total = this.orderService.item.sub_total - this.orderService.item.total_discount;
  }
}
