import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {ListRequest} from "../../../models/list-request";
import {OrderService} from "../../../services/order.service";
import {LoaderService} from "../../../services/loader.service";


@Component({
  selector: 'client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientListComponent implements OnInit {
  list = [];
  totalCount = 0;
  locationPath: string;

  scrollCallback;

  listRequest = new ListRequest();

  constructor(public clientService: ClientService,
              private orderService: OrderService,
              private loaderService: LoaderService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {
    this.locationPath = location.path(true);

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit() {
  }

  getListing() {
    if (!this.clientService.list.length || this.clientService.listCount - 1 >= this.clientService.list.length) {
      return this.clientService.getList(this.listRequest.getJSON())
        .do(this.processData)
    }

    return null;
  }

  public searchFromListing(search) {
    this.listRequest.clear();
    this.listRequest.search = search;
    this.clientService.list = [];
    this.getListing().subscribe();
  }

  private processData = (res) => {
    this.listRequest.page++;
  };

  public selectClient = (item?) => {
    const selectedId = item && item.id;

    this.orderService.createOrder({client: selectedId})
      .subscribe(res => {

        this.clientService.selectItem(item);

        this.router.navigate(['dashboard/clients'], {
          queryParams: {
            'left-view': 'client',
            'clientId': selectedId,
            'orderId': res.id
          }
        });

      });

  };
}
