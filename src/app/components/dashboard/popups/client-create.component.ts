import {Component, OnInit, Input} from '@angular/core';
import {Location} from '@angular/common';
import {Client} from "../../../models/client";
import {ClientService} from "../../../services/client.service";
import {LoaderService} from "../../../services/loader.service";
import * as commonHelper from "../../../helper/common-helper";
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms'
import 'rxjs/Rx';
import {OrderService} from "../../../services/order.service";
import {ActivatedRoute} from "@angular/router";
@Component({
  selector: 'popup-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./popup.component.css']
})
export class ClientCreateComponent implements OnInit {
  form: FormGroup;
  name = new FormControl('', Validators.required);
  phone = new FormControl('', Validators.required);
  address = new FormControl('', Validators.required);
  client_type = new FormControl('', Validators.required);
  item = new Client();
  response = {};

  constructor(private location: Location,
              private loaderService: LoaderService,
              private orderService: OrderService,
              private route: ActivatedRoute,
              public clientService: ClientService,
              formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      "name": this.name,
      "phone": this.phone,
      "address": this.address,
      "client_type": this.client_type
    });
    // this.form.valueChanges
    //   .filter(data => this.form.valid)
    //   .map(data => {
    //     // data.phone = data.phone.replace(/<(?:.|\n)*?>/gm, '');
    //     return data;
    //   })
    //   .subscribe(data => console.log(JSON.stringify(data)));

  }

  ngOnInit() {
  }

  onSubmit() {
    this.loaderService.toggleLoader(true);

    this.clientService.createItem(this.form.getRawValue())
      .subscribe(value => {

          const orderId = this.route.snapshot.queryParams["orderId"];
          if (orderId) {
            this.orderService.changeOrder(orderId, {client: {id: value.id}},
              res => {
                this.close();
              });
          }else{
            this.clientService.getList().subscribe();
          }
        },
        error => {
          this.loaderService.toggleLoader(false);
          this.response = JSON.parse(error._body);
        },
        () => this.close()
      );
  }


  close(e?) {
    if (e) {
      if (e.target.className.indexOf('popup-wrapper') > -1) {

        this.reset();
      }
    } else {
      this.reset();

    }
    return null;
  }

  reset() {
    this.loaderService.toggleLoader(false);
    this.clientService.toggleCreatePopup(false);
    this.form.reset();
  }
}
