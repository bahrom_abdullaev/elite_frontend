import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';
import fp from 'lodash/fp';

import {ClientService} from '../../../services/client.service'
import {InventoryService} from '../../../services/inventory.service';
import {Observable} from "rxjs/Observable";
import {ListRequest} from "../../../models/list-request";
import {OrderService} from "../../../services/order.service";
import {ReminderService} from "../../../services/reminder.service";


@Component({
  selector: 'popup-reminder-list',
  templateUrl: './reminder-list.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupReminderListComponent implements OnInit , OnDestroy{

  list = [];
  scrollCallback;

  listRequest = new ListRequest();

  constructor(public clientService: ClientService,
              public reminderService: ReminderService,
              private orderService: OrderService,
              private route: ActivatedRoute,
              private router: Router,
              public location: Location) {

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit() {
  }
  ngOnDestroy(): void {
    this.reminderService.list = [];
  }

  getListing() {
    const list = this.reminderService.list;
    if (!list.length || this.reminderService.listCount - 1 >= list.length) {
      const payload = this.listRequest.getJSON();
      payload['order'] = this.orderService.item.id;
      return this.reminderService.getList(payload)
        .do(this.processData);
    }
    return null;
  }

  public searchFromListing(search) {
    this.listRequest.clear();
    this.listRequest.search = search;
    this.reminderService.list = [];
    this.getListing().subscribe();
  }

  private processData = (list) => {
    this.listRequest.page++;
  };

  closePopup(e?) {
    if (e) {
      if (e.target.className.indexOf('popup-wrapper') > -1) {
        this.reminderService.toggleListPopup(false);
      }
    } else {
      this.reminderService.toggleListPopup(false);
    }
    return null;
  }

  public selectItem = (item) => {
    // const selectedId = item && item.id;
    this.reminderService.selectItem(item);
  };
}
