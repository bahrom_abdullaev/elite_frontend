import {Component, OnInit, OnDestroy} from '@angular/core';
import {Location} from '@angular/common';
import {Client} from "../../../models/client";
import {ClientService} from "../../../services/client.service";
import {LoaderService} from "../../../services/loader.service";
import * as commonHelper from "../../../helper/common-helper";
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms'
import 'rxjs/Rx';
import {OrderService} from "../../../services/order.service";
import {ExchangeService} from "../../../services/exchange.service"
import {ProductService} from "../../../services/product.service";
import {InventoryService} from "../../../services/inventory.service";
import * as _ from "lodash";
import {get as storageGet} from "../../../helper/storage-helper";
import {SELECTED_STORE} from "../../../constants/app.constants";
import {StoreService} from "../../../services/store.service";
declare var $: any;

@Component({
  selector: 'popup-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupAddProductComponent implements OnInit, OnDestroy {


  form: FormGroup;
  quantity = new FormControl(1, Validators.required);
  discount = new FormControl(0, Validators.required);
  response = {};

  constructor(private location: Location,
              private loaderService: LoaderService,
              private clientService: ClientService,
              private storeService: StoreService,
              private inventoryService: InventoryService,
              public exchangeService: ExchangeService,
              public productService: ProductService,
              public orderService: OrderService,
              formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      "quantity": this.quantity,
      "discount": this.discount,
    });
  }

  ngOnInit() {
    const orderItem = this.orderService.productOrderItem;
    const hasOrder = Boolean(this.orderService.item);
    const path = this.location.path();
    this.orderService.productOrderItem.viewMode = !Boolean(path.indexOf('left-view=client') > -1 && path.indexOf('orderId=') > -1);
    if (Boolean(orderItem)) {
      this.inventoryService.getProductAmount({
        product: orderItem.product['id'],
        store: storageGet(SELECTED_STORE).id,
        in: 'store'
      })
        .subscribe(res => {
          this.orderService.productOrderItem['in_store'] = res.in_store;
          this.orderService.productOrderItem['in_warehouse'] = res.in_warehouse;
        });
      if (Boolean(orderItem.id)) {
        this.quantity.setValue(orderItem.quantity);
        this.discount.setValue(_.round(this.exchangeService.getSOM(orderItem.discount)));
      }
    }
    setTimeout(() => {
      $("#quantityField").focus();
    }, 10);
  }

  ngOnDestroy(): void {
    this.orderService.productOrderItem = null;
  }

  onSubmit() {
    const orderItem = this.orderService.productOrderItem;
    const orderId = this.orderService.item.id;
    const isOrderItem = Boolean(orderItem && orderItem['order'] && orderItem['order']['id']);
    const orderItemId = orderItem && orderItem.id;
    const form = this.form.getRawValue();
    const payload = {...form};
    if (payload.discount) {
      payload.discount = this.exchangeService.getUSD(payload.discount);
    }
    const product = this.orderService.productOrderItem.product;
    payload.product = product['id'];
    // payload.exchange = this.exchangeService.primaryExchange.id;
    // payload.order = this.orderService.item.id;

    const saveStream = isOrderItem ? this.orderService.changeOrderItem(orderId, orderItemId, payload)
      : this.orderService.createOrderItems(orderId, payload);
    saveStream
      .subscribe(
        value => value,
        error => {
          try {
            this.loaderService.toggleLoader(false);
            this.response = JSON.parse(error._body);
          } catch (e) {
            console.warn(e);
          }
        },
        () => {
          this.orderService.getOrder(orderId);
          // this.orderService.getOrderItems(orderId)
          //   .subscribe();
          this.close();
        }
      );
  }

  toggleQuantity(positive: boolean) {
    const quantityValue = (+this.quantity.value) || 0;
    if (positive && quantityValue >= this.orderService.productOrderItem['in_store']) {
      return;
    }
    this.quantity.setValue(positive ? +quantityValue + 1 : +quantityValue - 1);
  }

  toggleDiscount(positive: boolean) {
    if (positive && this.discount.value + 1000 >= this.exchangeService.getSOM(this.orderService.getItemPrice())) {
      return;
    }
    this.discount.setValue(positive ? (+this.discount.value) + 1000 : (+this.discount.value) - 1000);
  }

  close(e?) {
    if (e) {
      if (e.target.className.indexOf('popup-wrapper') > -1) {
        this.reset();
      }
    } else {
      this.reset();
    }
    return null;
  }

  reset() {
    this.loaderService.toggleLoader(false);
    this.orderService.toggleOrderItemsPopup(false);
    this.quantity.setValue(1);
    this.discount.setValue(0);
    // this.location.back();
  }

  delete(item_id) {
    this.loaderService.toggleLoader(false);
    this.orderService.deleteOrderItem(this.orderService.item.id, item_id).subscribe(res => {
      this.close();
        this.orderService.getOrder(this.orderService.item.id);
    });
  }
}
