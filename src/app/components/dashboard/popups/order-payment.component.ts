import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Client} from "../../../models/client";
import {ClientService} from "../../../services/client.service";
import {LoaderService} from "../../../services/loader.service";
import * as commonHelper from "../../../helper/common-helper";
import {formatMoney, parseMoney} from "../../../helper/parser-helper";
import {ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms'
import 'rxjs/Rx';
import * as _ from "lodash/fp";
import {format} from 'date-fns';
import {OrderService} from "../../../services/order.service";
import {ExchangeService} from "../../../services/exchange.service"
import {
  DATE_FORMAT,
  PaymentMethod,
  PaymentStatus,
  PaymentType,
  STANDARD_FORMAT
} from "../../../constants/app.constants";
import {Router} from "@angular/router";
// const parseToArray = fp.flow(fp.valuesIn));
@Component({
  selector: 'order-payment',
  templateUrl: './order-payment.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupOrderPaymentComponent implements OnInit {
  form: FormGroup;
  amount = new FormControl('', Validators.required);
  due_date = new FormControl(format(new Date(), STANDARD_FORMAT), Validators.required);
  note = new FormControl('');
  type = new FormControl(PaymentMethod.CASH, Validators.required);
  addToReminder = new FormControl(false);
  response = {};
  paymentType = PaymentType.PARTIAL;
  payMethods = [];
  isFromPrintButton = false;

  constructor(private location: Location,
              private loaderService: LoaderService,
              private clientService: ClientService,
              private router: Router,
              private exchangeService: ExchangeService,
              public orderService: OrderService,
              formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      "amount": this.amount,
      "due_date": this.due_date,
      "note": this.note,
      "type": this.type,
      "addToReminder": this.addToReminder
    });
  }

  ngOnInit() {
    // const orderItem = this.orderService.productOrderItem;
    this.payMethods = _.valuesIn(PaymentMethod);
  }

  onSubmit() {
    if (this.form.valid) {
      const orderId = this.orderService.item.id;
      const payload = this.form.value;
      if (payload.due_date && this.paymentType != 'full') {
        payload.due_date = format(payload.due_date, DATE_FORMAT);
      } else {
        delete payload.due_date;
      }
      if (payload.addToReminder) {
        payload.status = PaymentStatus.PENDING;
      } else {
        payload.status = PaymentStatus.PAID;
      }
      if (payload.amount) {
        payload.amount = this.exchangeService.getUSD(parseMoney(payload.amount));
      }
      return this.orderService.createPayment(orderId, payload)
        .subscribe(
          value => {


            this.router.navigate(['dashboard']);
          },
          error => {
            try {
              this.loaderService.toggleLoader(false);
              this.response = JSON.parse(error._body);
            } catch (e) {
              console.warn(e);
            }
          },
          () => {
            this.orderService.getOrder(orderId);
            if (this.isFromPrintButton) {
              this.isFromPrintButton = false;
              this.openPrint();
            }
            // this.orderService.getOrderItems(orderId);
            this.close();
          });
    } else {
      this.form.updateValueAndValidity();
    }
    // console.log(this.form);
  }

  onClickPartialPaid() {
    this.paymentType = PaymentType.PARTIAL;

    this.amount.setValue('');
  }

  openPrint() {
    window.open(`http://qa-manager.aksessuar.uz/orders/${this.orderService.item.id}/receipt_wl`);
  }

  onClickFulPaid() {
    this.paymentType = PaymentType.FULL;
    this.addToReminder.setValue(false);
    if (this.orderService.item) {
      const paymentPrice = this.orderService.item.total - this.orderService.item.total_payment;
      this.amount.setValue(formatMoney(this.exchangeService.getSOM(paymentPrice)));
    }
  }

  close(e?) {
    if (e) {
      if (typeof e.target.className == 'string' &&
        e.target.className.indexOf('popup-wrapper') > -1) {
        this.reset();
      }
    } else {
      this.reset();
    }
    return null;
  }

  printAndGoDashboard(isPrint) {
    this.isFromPrintButton = true;
    if (this.form.valid) {
      this.onSubmit();
    }
  }

  reset() {
    this.loaderService.toggleLoader(false);
    this.orderService.togglePaymentPopup(false);
    this.amount.setValue(0);
    this.due_date.setValue(new Date());
    this.note.setValue('');
    // this.location.back();
  }
}
