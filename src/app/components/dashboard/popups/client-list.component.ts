import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mapTo';

import {ClientService} from '../../../services/client.service'
import {ListRequest} from "../../../models/list-request";
import {OrderService} from "../../../services/order.service";


@Component({
  selector: 'popup-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupClientListComponent implements OnInit {
  list = [];

  scrollCallback;

  listRequest = new ListRequest();

  constructor(public clientService: ClientService,
              private orderService: OrderService,
              private route: ActivatedRoute,
              public location: Location) {

    this.scrollCallback = this.getListing.bind(this);
  }

  ngOnInit() {
  }

  getListing() {
    const list = this.clientService.list;
    if (!list.length || this.clientService.listCount - 1 >= list.length) {
      return this.clientService.getList(this.listRequest.getJSON())
        .do(this.processData);
    }
    return null;
  }

  public searchFromListing(search) {
    this.listRequest.clear();
    this.listRequest.search = search;
    this.clientService.list = [];
    this.getListing().subscribe();
  }

  private processData = (list) => {
    this.listRequest.page++;
  };

  closePopup(e?) {
    if (e) {
      if (e.target.className.indexOf('popup-wrapper') > -1) {
        this.clientService.toggleListPopup(false);
      }
    } else {
      this.clientService.toggleListPopup(false);
    }
    return null;
  }

  public selectClient = (item) => {
    const selectedId = item && item.id;
    this.clientService.selectItem(item);

    if (Boolean(selectedId)) {
      const orderId = this.route.snapshot.queryParams["orderId"];
      this.orderService.changeOrder(orderId, {client: {id: item.id}},
        res => {
          this.closePopup();
        });
    }
  };
  public openCreatePopup = () => {
    this.closePopup();
    this.clientService.toggleCreatePopup(true);
  }
}
