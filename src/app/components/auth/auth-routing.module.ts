import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from '../../services/auth-guard.service'

import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {ChooseStoreComponent} from './choose-store/choose-store.component';
import {ChooseUserComponent} from './choose-user/choose-user.component';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent,
    canLoad: [AuthGuard],
    children: [
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'choose-store',
        component: ChooseStoreComponent
      },
      {
        path: 'choose-user',
        component: ChooseUserComponent
      },
    ]
  }

];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
