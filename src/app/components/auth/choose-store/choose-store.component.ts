import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';

import * as Constants from '../../../constants/app.constants';
import * as storage from '../../../helper/storage-helper';
import {Store} from '../../../models/store';
import {StoreService} from '../../../services/store.service';
import {LoaderService} from "../../../services/loader.service";


@Component({
  selector: 'app-choose-store',
  templateUrl: './choose-store.component.html',
  styleUrls: ['./choose-store.component.css']
})
export class ChooseStoreComponent implements OnInit {
  list: Observable<Store[]>;

  private selectedId: number;
  private selectedItem: Store;

  constructor(public storeService: StoreService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.storeService.getList()
      .subscribe();
  }

  isSelected(item: Store) {
    return this.selectedItem && item.id == this.selectedItem.id;
  }

  onSelect(item: Store) {
    storage.save(Constants.SELECTED_STORE, item);
    this.storeService.selectItem(item);
    this.router.navigateByUrl('/auth/choose-user');
  }
}
