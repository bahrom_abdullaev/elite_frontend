import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';

import * as Constants from '../../../constants/app.constants'
import * as storage from '../../../helper/storage-helper';
import {User} from '../../../models/user';
import {EmployeeService} from '../../../services/employee.service';
import {LoaderService} from "../../../services/loader.service";
import {StoreService} from "../../../services/store.service";


@Component({
  selector: 'app-choose-user',
  templateUrl: './choose-user.component.html',
  styleUrls: ['./choose-user.component.css']
})
export class ChooseUserComponent implements OnInit {
  list: Observable<User[]>;

  private selectedId: number;
  private selectedItem: User;

  constructor(public employeeService: EmployeeService,
              private storeService: StoreService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    if (this.storeService.item && this.storeService.item.id) {
      this.employeeService.getList({store: this.storeService.item.id})
        .subscribe();
    } else {
      this.router.navigateByUrl('/auth/choose-store');
    }
  }

  isSelected(item: User) {
    return this.selectedItem && item.id == this.selectedItem.id;
  }

  onSelect(item: User) {
    this.employeeService.selectItem(item);
    this.router.navigateByUrl('/auth');
  }
}
