import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from "@angular/router";
import fp from 'lodash/fp';
import * as storage from '../../../helper/storage-helper'
import * as Constants from '../../../constants/app.constants'
import {EmployeeService} from "../../../services/employee.service";
declare var $: any;

const NUMBER_BUTTONS = fp.concat(fp.range(1, 10), 0);
const DEFAULT_PASSWORD = '1230';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  message: string;
  buttons = NUMBER_BUTTONS;
  password = '';

  constructor(private authService: AuthService,
              public employeeService: EmployeeService,
              private router: Router) {
  }

  setMessage() {
    this.message = this.authService.isLoggedIn ? 'ON' : 'OFF';
  }

  ngOnInit() {
    setTimeout(()=>{
      $('#passwordField').focus();
    })
  }

  login() {
    this.message = 'Loading';

    const user = storage.get(Constants.SELECTED_USER);
    const request = {
      username: user.username,
      password: this.password
    };

    this.authService.login(request)
      .subscribe(token => {
        this.setMessage();

        this.authService.redirectUrl = '/dashboard';
        this.authService.isLoggedIn = Boolean(token);
        storage.save(Constants.TOKEN, token);

        this.router.navigateByUrl(this.authService.redirectUrl);
      });
  }

  onButtonPress(item) {
    this.password += ('' + item);
  }

  onRemoveNumber() {
    this.password = this.password.slice(0, this.password.length - 1);
  }


}
