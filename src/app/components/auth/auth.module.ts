import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AuthRoutingModule} from './auth-routing.module';
import {AuthComponent} from './auth.component';
import {ChooseStoreComponent} from './choose-store/choose-store.component';
import { LoginComponent } from './login/login.component';
import { ChooseUserComponent } from './choose-user/choose-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule
  ],
  declarations: [
    AuthComponent,
    ChooseStoreComponent,
    LoginComponent,
    ChooseUserComponent
  ]
})
export class AuthModule {
}
