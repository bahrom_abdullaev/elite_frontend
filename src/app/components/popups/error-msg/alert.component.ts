import {Component, OnInit, Input} from '@angular/core';
import {Location} from '@angular/common';
import {Client} from "../../../models/client";
import {LoaderService} from "../../../services/loader.service";
import {PopupService} from "../../../services/popup.service";

@Component({
  selector: 'popup-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class PopupAlertComponent implements OnInit {

  constructor(private location: Location, public popupService: PopupService) {
  }

  ngOnInit() {

  }


  closePopup(e?) {
    if (e) {
      if (e.target.className.indexOf('popup-wrapper') > -1) {
        this.popupService.toggle(false);
      }
    } else {
      this.popupService.toggle(false);
    }
    return null;
  }
}
