import {Component, OnInit, Input} from '@angular/core';
import {Location} from '@angular/common';
import {Client} from "../../../models/client";
import {LoaderService} from "../../../services/loader.service";

@Component({
  selector: 'popup-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor(private location: Location, private loaderService:LoaderService) {
  }

  ngOnInit() {

  }


  close() {
    this.location.back();
  }

  isActivePopup() {
      return this.loaderService.loader;
  }
}
