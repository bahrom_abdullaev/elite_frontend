import {Component, OnInit} from '@angular/core';
declare var jquery: any;
declare var $: any;
import {differenceInDays} from "date-fns";
import {split, join, flow, reverse} from "lodash/fp";
import {ReminderService} from '../../services/reminder.service';
import {AuthService} from '../../services/auth.service';

const parseAsDateFormat = flow(
  split('.'),
  reverse,
  join('/')
);
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  constructor(public reminderService: ReminderService,
              public authService: AuthService) {
  }

  ngOnInit() {
    let clock;

    $(document).ready(function () {
      clock = $('.clock').FlipClock({
        clockFace: 'TwentyFourHourClock'
      });
    });

    this.reminderService.getList()
      .subscribe(val => {

      });
  }

  getLeftDay(item) {
    const formattedDate = parseAsDateFormat(item.due_date);
    const dueDate = new Date(formattedDate);
    const now = new Date();
    const leftDays = differenceInDays(now, dueDate);
    let style = 'simple';

    if (leftDays >= 3 && leftDays < 5) {
      style = 'blue';
    } else if (leftDays >= 2 && leftDays < 3) {
      style = 'red';
    } else if (leftDays >= 0 && leftDays < 2) {
      style = 'pink';
    }

    return style;
  }

}
