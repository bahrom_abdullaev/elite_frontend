import {Pipe, PipeTransform} from '@angular/core';
import {currencyMoney} from "../helper/parser-helper";
import {OrderService} from "../services/order.service";
import {ExchangeService} from "../services/exchange.service";

@Pipe({
  name: 'eliteMoney'
})
export class EliteMoneyPipe implements PipeTransform {
  constructor(public exchangeService: ExchangeService) {}

  transform(value: any, args?: any): string {
    const exchange = this.exchangeService.primaryExchange ? this.exchangeService.primaryExchange.summ : 1;
    return currencyMoney(exchange * parseFloat(value));
  }

}
