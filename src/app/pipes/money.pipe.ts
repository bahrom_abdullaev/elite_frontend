import {Pipe, PipeTransform} from '@angular/core';
import {currencyMoney} from '../helper/parser-helper';

@Pipe({
  name: 'money'
})
export class MoneyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return currencyMoney(value);
  }

}
