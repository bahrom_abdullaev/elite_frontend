import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {handleError} from '../helper/http-helper';
import {HOST, SELECTED_STORE} from '../constants/app.constants';
import {get as getKey} from '../helper/storage-helper';
import {Store} from '../models/store';
import {LoaderService} from "./loader.service";

interface ItemsResponse {
  results: string[];
}

const STORE_LIST = [
  new Store(11, 'Mr. Nice'),
  new Store(12, 'Narco'),
  new Store(13, 'Bombasto'),
  new Store(14, 'Celeritas'),
  new Store(15, 'Magneta'),
  new Store(16, 'RubberMan')
];

@Injectable()
export class StoreService {
  list = [];
  item: Store = getKey(SELECTED_STORE);

  constructor(private baseService: BaseService,
              private loaderService: LoaderService) {
  }

  getList() {
    return this.baseService.getStream('/stores')
      .map(response => response.json().results as Store[])
      .map(res => {
        this.list = res;
      })
  }


  getItem(id: number | string) {
    return this.baseService.getStream(`/stores/${id}`)
      .map(res => res.json().data as Store)
  }

  selectItem(item) {
    this.item = item;
  }
}
