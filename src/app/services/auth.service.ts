import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';
import {Router} from "@angular/router";


@Injectable()
export class AuthService {
  token = storage.get(TOKEN);
  isLoggedIn = Boolean(this.token);

  redirectUrl = '/dashboard';

  constructor(private baseService: BaseService,
              private router: Router) {
  }

  login(data) {
    return this.baseService.postStream(`/users/auth/`, data)
      .map(response => response.json().token);
  }

  logout(): void {
    this.isLoggedIn = false;
    storage.remove(TOKEN);
    this.router.navigate(['/welcome']);
  }
}
