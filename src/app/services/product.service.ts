import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import {TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';
import {BaseService} from "./base.service";
import {Product} from "../models/product";


@Injectable()
export class ProductService {
  item: Product;

  constructor(private baseService: BaseService) {
  }

  getItem(id, data?): any {
    return this.baseService.getStream(`/products/${id}/`, data)
      .map(response => response.json())
      .do(res => {
        this.item = res;
        return res;
      })
  }
}
