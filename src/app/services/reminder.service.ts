import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {User} from '../models/user';

@Injectable()
export class ReminderService {
  list = [];
  listCount = 0;
  listPopup = false;
  selectedItem;

  constructor(private baseService: BaseService) {
  }

  getList(data?): any {
    return this.baseService.getStream(`/reminders/red_list/`, data)
      .map(res => res.json())
      .do(res => {
        if (!data || data.page <= 1) {
          this.listCount = res.count;
          this.list = [];
        }
        this.list = this.list.concat(res.results);

        return res;
      });
  }

  getItem(id: number | string) {
    return this.baseService.getStream(`/users/` + id)
      .map(res => res.json().data as User)
  }

  selectItem(item?) {
    this.selectedItem = item;
  }

  toggleListPopup(visible) {
    this.listPopup = visible
  }
}
