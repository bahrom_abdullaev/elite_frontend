import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import {TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';
import {Router} from "@angular/router";


@Injectable()
export class CommonService {

  constructor(private router:Router) {
  }

  decodeURI(path) {
    return decodeURIComponent(path);
  }
  getCurrentPath(){
    return decodeURI(this.router.url.toString());
  }
}
