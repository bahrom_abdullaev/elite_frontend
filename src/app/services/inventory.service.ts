import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class InventoryService {
  productList = [];
  productCount = 0;
  selectedCategory: any;
  selectedSubCategory: any;
  inventoryItem: any;
  listPayload;

  constructor(private baseService: BaseService) {
  }

  getInventoryList(data?): any {
    this.listPayload = data;
    return this.baseService.getStream(`/inventory/`, data)
      .map(response => response.json())
      .do(res => {
        if (!data || data.page == 1) {
          this.productCount = res.count;
          this.productList = [];
        }
        this.productList = this.productList.concat(res.results);
        return res;
      })
  }

  getCategoryList(data?): any {
    return this.baseService.getStream(`/inventory/categories/`, data)
      .map(response => response.json().results)
  }

  getProductAmount(data?): any {
    return this.baseService.getStream(`/inventory/dashboard_product_amounts/`, data)
      .map(response => response.json())
  }


  getSubCategoryList(data?): any {
    return this.baseService.getStream(`/inventory/subcategories/`, data)
      .map(response => response.json().results)
  }

  selectCategory(item: any, isSub = false) {
    if (isSub) {
      this.selectedSubCategory = item;
    } else {
      this.selectedCategory = item;
    }
  }

}
