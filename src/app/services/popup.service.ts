import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import {TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';


@Injectable()
export class PopupService {
  public visible = false;
  public title: string = '';
  public message: string = '';

  constructor() {
  }

  toggle(visible, title = '',message='') {
    this.visible = visible;
    this.title = title;
    this.message = message;
  }
}
