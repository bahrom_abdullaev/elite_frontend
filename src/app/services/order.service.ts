import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {LoaderService} from "./loader.service";
import {Order} from "../models/order";
import {OrderItem} from "../models/order-item";
import {MANAGER_URL} from "../constants/app.constants";

@Injectable()
export class OrderService {
  public item: Order;
  public list = [];
  public listCount = 0;
  public orderItemsPopup = false;
  public paymentPopup = false;
  public productOrderItem: OrderItem;
  public paymentItem;
  public orderItems = [];
  public orderItemsCount = 0;


  constructor(private baseService: BaseService) {
  }

  getList(data?): any {
    return this.baseService.getStream(`/orders/`, data)
      .map(response => response.json())
      .do(response => {
        const list = response.results;
        if (!data || data.page <= 1) {
          this.listCount = response.count;
          if (!Boolean(this.item)) {
            this.item = list[0];
            this.getOrder(list[0].id)
          }
        }
        this.list = this.list.concat(list);

        return list;
      });
  }

  createOrder(data?): any {
    return this.baseService.postStream(`/orders/`, data)
      .map(response => response.json() as Order)
      .map(response => {
        this.item = response;
        return this.item;
      });
  }

  changeOrder(id, data?, callback?): any {
    return this.baseService.putStream(`/orders/${id}/`, data)
      .map(response => response.json() as Order)
      .subscribe(res => {
        this.getOrder(id);
        if (callback) callback(res);

        return res;
      });
  }

  createPayment(id, data?): any {
    return this.baseService.postStream(`/orders/${id}/payments/`, data)
      .map(response => {
        this.paymentItem = response.json();
        return this.paymentItem;
      });
  }

  getOrder(id?, callback?): any {
    return this.baseService.getStream(`/orders/${id}/`)
      .map(res => res.json() as Order)
      .subscribe(res => {
        this.item = res;
        if (this.list && this.list.length) {
          let foundIndex = this.list.findIndex(listItem => listItem.id == res.id);
          this.list[foundIndex] = res;
        }
        if (!res.total_discount) this.item.total_discount = 0;
        if (!res.total_payment) this.item.total_payment = 0;

        this.orderItemsCount = res.order_items ? res.order_items.length : 0;
        this.orderItems = res.order_items || [];
        if (callback) callback(res);
      });
  }

  getOrderItems(id, data?): any {
    return this.baseService.getStream(`/orders/${id}/items/`, data)
      .map(response => response.json())
      .do(res => {
        if (!data || data.page <= 1) {
          this.orderItemsCount = res.count;
          this.orderItems = [];
        }
        this.orderItems = this.orderItems.concat(res.results);

        return res;
      })
  }

  getOrderItemsItem(orderId, id): any {
    return this.baseService.getStream(`/orders/${orderId}/items/${id}/`)
      .map(response => response.json());
  }

  getOrderReceipt(orderId): any {
    return this.baseService.getStream(`/orders/${orderId}/reciept`)
      .map(response => response.json());
  }

  createOrderItems(orderId, data ?): any {
    this.baseService.showLoader();
    return this.baseService.postStream(`/orders/${orderId}/items/`, data)
      .map(response => response.json());
  }

  changeOrderItem(orderId, id, data ?): any {
    this.baseService.showLoader();
    return this.baseService.putStream(`/orders/${orderId}/items/${id}/`, data)
      .map(response => response.json());
  }

  deleteOrderItem(orderId, id, data ?): any {
    this.baseService.showLoader();
    return this.baseService.delete(`/orders/${orderId}/items/${id}/`, data);
  }

  toggleOrderItemsPopup(visible) {
    this.orderItemsPopup = visible;
    if (!visible) {
      this.productOrderItem = null;
    }
  }

  togglePaymentPopup(visible) {
    this.paymentPopup = visible;
  }

  openOrderItemsPopup(item) {
    this.productOrderItem = item;
    // if (this.productOrderItem) this.productOrderItem['amount'] = 0;
    this.toggleOrderItemsPopup(true);
  }

  selectOrderItem(item) {
    this.item = item;
    this.orderItems = [];
  }

  setOrderItems(list = []) {
    this.orderItems = list;
  }

  printOrderCheck() {
    window.open(`${MANAGER_URL}orders/${this.item.id}/receipt_wl`);
  }

  getItemPrice(productItem?) {
    const productOrderItem = productItem || this.productOrderItem;
    const hasOrder = Boolean(this.item);
    const hasClient = hasOrder && Boolean(this.item.client);
    const isWhole = hasClient && this.item.client['client_type'] == "WHOLESALE";
    const isOrderItem = Boolean(productOrderItem.order && productOrderItem.order.id);
    return isOrderItem ? productOrderItem['price'] :
      productOrderItem[isWhole ? 'wholesale_price' : 'retail_price'];
  }

}
