import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CategoryService {
  item: any;

  constructor(private baseService: BaseService) {
  }

  getItem(id, data?): any {
    return this.baseService.getStream(`/categories/${id}/`, data)
      .map(response => response.json())
  }

  selectCategory(item: any, isSub = false) {
    if (isSub) {
      this.item = item;
    } else {
      this.item = item;
    }
  }

}
