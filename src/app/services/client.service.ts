import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {BaseService} from './base.service';
import {Client} from '../models/client';
import {LISTING_REQUEST} from "../constants/app.constants";

@Injectable()
export class ClientService {
  selectedClient: Client;
  listPopup: false;
  createPopup: false;
  list = [];
  listCount = 0;

  constructor(private baseService: BaseService) {
  }

  getList(payload = LISTING_REQUEST): any {
    return this.baseService.getStream(`/clients/`, payload)
      .map(response => response.json())
      .do(response => {
        if (!payload || payload.page <= 1) {
          this.listCount = response.count;
          this.list = [];
        }
        this.list = this.list.concat(response.results);
        return response;
      })
  }

  getItem(id: number | string) {
    return this.baseService.get(`/clients/` + id)
      .then(res => res.json().data);
  }

  createItem(data: Client) {
    return this.baseService.postStream(`/clients/`, data)
      .map(res => res.json());
  }

  selectItem(item: Client) {
    this.selectedClient = item;
  }

  toggleListPopup(visible) {
    this.listPopup = visible;
  }
  toggleCreatePopup(visible) {
    this.createPopup = visible;
  }

}
