import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {User} from '../models/user';
import {Observable} from "rxjs/Observable";
import {SELECTED_USER} from "../constants/app.constants";
import * as storage from "../helper/storage-helper";

@Injectable()
export class EmployeeService {
  list = [];
  item: User = storage.get(SELECTED_USER);

  constructor(private baseService: BaseService) {
  }

  getList(data?): any {
    return this.baseService.getStream(`/users/employee/`, data)
      .map(response => response.json().results as User[])
      .map(res => {
        this.list = res;
      });
  }

  getItem(id: number | string) {
    return this.baseService.getStream(`/users/` + id)
      .then(res => res.json().data as User)
  }

  selectItem(item) {
    storage.save(SELECTED_USER, item);
    this.item = item;
  }
}
