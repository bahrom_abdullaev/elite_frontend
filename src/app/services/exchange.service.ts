import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {Observable} from "rxjs/Observable";
import * as storage from "../helper/storage-helper";
import {currencyMoney} from "../helper/parser-helper";
import {PRIMARY_EXCHANGE_VALUE} from "../constants/app.constants";
import * as _ from "lodash";
@Injectable()
export class ExchangeService {
  primaryExchange = storage.get(PRIMARY_EXCHANGE_VALUE) ||{summ:8000};

  constructor(private baseService: BaseService) {
  }

  getPrimaryExchange(): any {
    this.baseService.showLoader();
    return this.baseService.getStream(`/exchanges/get_primary/`)
      .map(response => response.json())
      .subscribe(val => {
        this.baseService.hideLoader();
        this.primaryExchange = val;
        storage.save(PRIMARY_EXCHANGE_VALUE, val);
      }, err => this.baseService.hideLoader());
  }

  getSOM(price = 0) {
    return price * (this.primaryExchange ? this.primaryExchange.summ : 8000);
  }

  getUSD(price = 0) {
    return price / (this.primaryExchange ? this.primaryExchange.summ : 8000);
  }
}
