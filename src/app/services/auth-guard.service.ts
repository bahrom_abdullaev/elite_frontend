import {Injectable}       from '@angular/core';
import fp from 'lodash/fp';
import {
  CanActivate, CanDeactivate, Router,
  CanLoad,
  ActivatedRouteSnapshot,
  RouterStateSnapshot, Route
}                           from '@angular/router';
import {AuthService}      from './auth.service';
import {Observable} from "rxjs/Observable";

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class AuthGuard implements CanActivate, CanLoad, CanDeactivate<CanComponentDeactivate> {
  constructor(private authService: AuthService, private router: Router) {
  }

  canLoad(route: Route): boolean {
    let url = `/${route.path}`;

    return this.checkLogin(url);
  }

  canDeactivate(component: CanComponentDeactivate) {
    return true;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this.authService.isLoggedIn) {
      return true;
    }

    // Store the attempted URL for redirecting
    this.authService.redirectUrl = url;

    // Navigate to the login page with extras
    this.router.navigate(['/auth']);
    return false;
  }
}
