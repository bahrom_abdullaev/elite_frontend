import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import 'rxjs/add/operator/toPromise';
import {User} from '../models/user';

@Injectable()
export class UserService {

  constructor(private baseService: BaseService) {
  }

  getList() {
    return this.baseService.get(`/users/me`)
      .then(response => response.json().results as User[])
  }

  getItem(id: number | string) {
    return this.baseService.get(`/users/` + id)
      .then(res => res.json().data as User)
  }

}
