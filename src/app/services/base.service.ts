import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {LoaderService} from './loader.service';
import {PopupService} from "./popup.service";
import {AuthService} from "./auth.service";

import {handleError} from '../helper/http-helper';
import {HOST, TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';
import * as commonHelper from '../helper/common-helper';
import {Router} from "@angular/router";

@Injectable()
export class BaseService {
  authService : AuthService;

  constructor(private http: Http,
              public loaderService: LoaderService,
              private router: Router,
              injector:Injector,
              public popupService: PopupService) {

    // setTimeout(() => this.authService = injector.get(AuthService));
  }

  getOptions(data?, forParams = false) {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    if (storage.get(TOKEN)) {
      headers.set('X-CSRFToken', storage.get(TOKEN));
      headers.set('Authorization', 'Token ' + storage.get(TOKEN));
      headers.set('app-name', 'dashboard_web_app');
      // 'AppName': 'dashboard_web_app',

    }
    const options = new RequestOptions({headers: headers});
    if (data && forParams) {
      options.params = data;
    }
    return options;
  }

  get(path, data ?): any {
    const url = `${HOST}${path}`;
    return this.http.get(url, this.getOptions(data, true))
      .toPromise()
      .then(data => {
        commonHelper.toggleLoader(false);
        return data;
      })
      .catch(handleError);
  }

  getStream(path, data ?, hasLoader = true): any {
    const url = `${HOST}${path}`;
    if (hasLoader) this.showLoader();
    const stream = this.http.get(url, this.getOptions(data, true)).share();
    stream.subscribe(val => this.hideLoader(), err => this.errorHandler(err));
    return stream;
  }

  postStream(path, data?, hasLoader = true): any {
    const url = `${HOST}${path}`;
    if (hasLoader) this.showLoader();
    const stream = this.http.post(url, JSON.stringify(data), this.getOptions())
      .share();
    stream.subscribe(val => this.hideLoader(), err => this.errorHandler(err));
    return stream;
  }

  putStream(path, data?, hasLoader = true): any {
    const url = `${HOST}${path}`;
    if (hasLoader) this.showLoader();
    const stream = this.http.put(url, JSON.stringify(data), this.getOptions())
      .share();
    stream.subscribe(val => this.hideLoader(), err => this.errorHandler(err));
    return stream;
  }

  post(path, data): any {
    const url = `${HOST}${path}`;
    return this.http.post(url, data, this.getOptions(data))
      .toPromise()
      .then(res => {
        commonHelper.toggleLoader(false);
        return res
      })
      .catch(handleError);
  }

  delete(path, data): any {
    const url = `${HOST}${path}`;
    return this.http.delete(url, this.getOptions(data));
  }

  put(path, data) {
    const url = `${HOST}${path}`;
    return this.http.put(url, this.getOptions(data))
      .toPromise()
      .then(res => {
        commonHelper.toggleLoader(false)
        return res
      })
      .catch(handleError);
  }

  errorHandler(err) {
    let message = '';
    this.hideLoader();
    if (!err.ok)
      try {
        const obj = JSON.parse(err._body);
        const errors = obj && obj['non_field_errors'];
        if (Array.isArray(errors)) {

          errors.forEach(item => {
            message += `<div>${item}</div>`
          });
        }

        if (err.status == 401 || err.status == 403) {
          storage.remove(TOKEN);
          this.router.navigate(['/welcome']);

          // this.authService.logout();
        }
      } catch (e) {
        console.warn(e);
      }

    if (message) {
      this.popupService.toggle(true, "Ошибка", message);
    }
  }

  public showLoader() {
    this.loaderService.toggleLoader(true);
  }

  public hideLoader() {
    this.loaderService.toggleLoader(false);
  }
}
