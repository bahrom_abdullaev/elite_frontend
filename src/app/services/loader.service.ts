import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import {TOKEN} from '../constants/app.constants';
import * as storage from '../helper/storage-helper';


@Injectable()
export class LoaderService {
  public loader = false;

  constructor() {
  }

  toggleLoader(visible) {
    this.loader = visible;
  }
}
