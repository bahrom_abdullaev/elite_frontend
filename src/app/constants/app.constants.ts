export const APP_NAME = 'ELITE';
export const APP_VERSION = '1.0.0';

const QA_HOST = 'http://qa-api.aksessuar.uz';
const LIVE_HOST = 'http://api.aksessuar.uz';

const QA_MANAGER_URL = 'http://qa-manager.aksessuar.uz/';
const LIVE_MANAGER_URL = 'http://manager.aksessuar.uz/';


export const HOST = LIVE_HOST;
export const MANAGER_URL = LIVE_MANAGER_URL;
//region <Storage keys>
export const SELECTED_STORE = 'SELECTED_STORE';
export const SELECTED_USER = 'SELECTED_USER';
export const TOKEN = 'TOKEN';
export const PRIMARY_EXCHANGE_VALUE = 'PRIMARY_EXCHANGE_VALUE';
//endregion

export const LISTING_PAGE_SIZE = 25;
export const LISTING_REQUEST = {page_size: LISTING_PAGE_SIZE, page: 1};

export const DATE_FORMAT = 'MM.DD.YYYY';
export const STANDARD_FORMAT = 'YYYY-MM-DD';

export const OrderStatus = {
  DRAFT: "DRAFT",
  CANCELLED: "CANCELLED",
  PARTIAL_PAID: "PARTIAL_PAID",
  COMPLETED: "COMPLETED"
};
export const TranslatedOrderStatus = {
  DRAFT: "Черновик",
  CANCELLED: "Отменен",
  PARTIAL_PAID: "Частичный платный",
  COMPLETED: "Законченный"
};
export const PaymentMethod= {
  CASH: "CASH",
  CARD: "CARD",
  TRANSFERS: "TRANSFERS"
};

export const PaymentStatus = {
  PAID: "PAID",
  PENDING: "PENDING",
  CANCELLED: "CANCELLED"
};

export const PaymentType = {
  PARTIAL: "partial",
  FULL: "full"
};
